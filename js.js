// --------------------- PHONE MASK (start ---------------------
var element = document.getElementById('phone');
var maskOptions = {
    mask: '+{7} (000) 000-00-00',
    lazy: true
}
var mask = new IMask(element, maskOptions);

const test = (e) =>{
  e.preventDefault();
  var number = element.value.replace(/\D+/g, '');
  var newNumber = 8 + number.slice(1)
  console.log(newNumber);

}

// // --------------------- PHONE MASK (end) ---------------------

// // --------------------- popup -------------------------------

// let openPopup = document.getElementsByClassName("popup");

// function addPopup() {
//   openPopup[0].classList.add("active");
//   document.body.classList.add("scroll-none");

//   document.getElementsByClassName("footer")[0].classList.add("blur");
//   document.getElementsByClassName("header")[0].classList.add("blur");
//   document.getElementsByClassName("main-content")[0].classList.add("blur");
// }

// function closePopup() {
//   openPopup[0].classList.remove("active");
//   document.body.classList.remove("scroll-none");
//   document.getElementsByClassName("footer")[0].classList.remove("blur");
//   document.getElementsByClassName("header")[0].classList.remove("blur");
//   document.getElementsByClassName("main-content")[0].classList.remove("blur");
// }

// document.onclick = function (e) {
//   if (e.target.className.includes("popup")) {
//     closePopup();
//   }
// };

// openPopup[0].childNodes[1].addEventListener("click", function (e) {
//   e.stopPropagation();
// });

// // ----------- burger menu -------------

// openBurger = document.getElementsByClassName("header__burger")

// function addBurger() {
//   openBurger[0].classList.toggle("active__burger");
//   document.getElementsByClassName("header")[0].classList.toggle("burger-menu");
//   document.body.classList.toggle("scroll-none");
// }

// -----------------------------------------------------------------------------------
var cover = document.getElementById("cover");
var cover__observer = document.getElementById("cover__observer");

var optionsObserv = {
  threshold: 0,
  rootMargin: "0px 0px 0px 0px",
};

var observer = new IntersectionObserver((entries) => {
  entries.forEach((entri) => {
    if (entri.isIntersecting) {
      document.body.style.overflow = "hidden";
      // cover.style.overflow = "hidden";
      window.scrollTo({ top: 0 });
      cover.classList.add("cover_hidden");
      cover.style.overflow = "hidden";
      setTimeout(() => {
        cover.style.display = "none";
        document.body.style.overflow = "auto";
      }, 5999);
    }
  });
}, optionsObserv);

observer.observe(cover__observer);
